provider "google" {
  region  = var.region
  project = var.project_id
  zone    = var.zone
}

data "google_client_config" "current" {
}

data "google_container_engine_versions" "default" {
  location = var.location
}
#module "storage" {
#  source     = "./modules/storage"
#}
