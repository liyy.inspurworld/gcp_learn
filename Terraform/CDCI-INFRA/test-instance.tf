resource "google_compute_instance" "tf-instance-1" {
    can_ip_forward       = false
    deletion_protection  = false
    enable_display       = false
    machine_type         = "e2-medium"
    metadata             = {}
    name                 = "instance-1"
    tags                 = ["nettag-kk123",]
    zone                 = "us-west1-a"
    allow_stopping_for_update = true
    boot_disk {
        initialize_params {
            image  = "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-10-buster-v20211209"
            labels = {}
            size   = 10
            type   = "pd-balanced"
        }
    }
    network_interface {
        network            = "https://www.googleapis.com/compute/v1/projects/my-project-91919-for-cicd/global/networks/default"
        subnetwork         = "https://www.googleapis.com/compute/v1/projects/my-project-91919-for-cicd/regions/us-west1/subnetworks/default"
        access_config {
            network_tier = "PREMIUM"
        }
    }
    network_interface {
        network            = "https://www.googleapis.com/compute/v1/projects/my-project-91919-for-cicd/global/networks/gcp-trust"
        subnetwork         = "https://www.googleapis.com/compute/v1/projects/my-project-91919-for-cicd/regions/us-west1/subnetworks/gcp-trust-subnet"
        access_config {
            network_tier = "PREMIUM"
        }
    }

    scheduling {
        automatic_restart   = true
        min_node_cpus       = 0
        on_host_maintenance = "MIGRATE"
        preemptible         = false
    }
}
