#!/usr/bin/env bash


set -x 
set -e

IPA=$(terraform output load-balancer-ip)
IPA=${IPA//'"'/}
URL="http://$IPA"
status=0
count=0
while [[ $count -lt 120 && $status -ne 200 ]]; do
  echo "INFO: Waiting for load balancer..."
  status=$(curl -sf -m 5 -o /dev/null -w "%{http_code}" "${URL}" || true)
#  status=$(curl -sf -m 5 -o /dev/null -w "%{http_code}" "http://35.233.215.18"  || true)
  ((count=count+1))
  sleep 5
done
if [[ $count -lt 120 ]]; then
  echo "INFO: PASS"
else
  echo "ERROR: Failed"
fi
