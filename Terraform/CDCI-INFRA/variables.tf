variable "region" {
  default = "us-west1"
}

variable "zone" {
  default = "us-west1-a"
}

variable "location" {
  default = "us-west1-a"
}

variable "network_name" {
  default = "tf-gke-k8s"
}

variable "project_id" {
 default = "my-project-91919-for-cicd"
}
